﻿// Matrix.cpp: определяет точку входа для консольного приложения.
//
#include "stdafx.h"
#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;
const int ArrayMinValue = -11;
const int ArrayMaxValue = 9;
const int NumberOfLine = 8;
const int NumberOfColumns = 8;

struct elementProperties
{
	int value;
	int lineCoordinate;
	int columnCoordinate;
};

int ReferenceArrayOfValues (int InputArray[][NumberOfColumns], int ArrayMinValue, int ArrayMaxValue);

int WriteArray (int InputArray[][NumberOfColumns]);

elementProperties FindPropertiesOfMinElementOnSecDiagonal (int InputArray[NumberOfLine][NumberOfColumns]);

elementProperties FindPropertiesOfMinElementUnderSecDiagonal (int InputArray[NumberOfLine][NumberOfColumns]);

void ReplaceElements (int InputArray[NumberOfLine][NumberOfColumns], elementProperties replacingElement, elementProperties replaceableElement);

void WritePropertiesOfElement (elementProperties Element, char specialProperty[]);

int main()
{
	srand(time(0));
	int NumerArray[NumberOfLine][NumberOfColumns];
	elementProperties minimalElementOnSecondaryDiagonal;
	elementProperties minimalElementUnderSecondaryDiagonal;
	ReferenceArrayOfValues (NumerArray, ArrayMinValue, ArrayMaxValue);
	WriteArray(NumerArray);
	minimalElementOnSecondaryDiagonal = FindPropertiesOfMinElementOnSecDiagonal(NumerArray);
	minimalElementUnderSecondaryDiagonal = FindPropertiesOfMinElementUnderSecDiagonal(NumerArray);
	WritePropertiesOfElement (minimalElementOnSecondaryDiagonal, "<minimal on secondary diagonal>");
	WritePropertiesOfElement(minimalElementUnderSecondaryDiagonal, "<minimal under secondary diagonal>");
	ReplaceElements(NumerArray, minimalElementOnSecondaryDiagonal, minimalElementUnderSecondaryDiagonal);
	WriteArray(NumerArray);
	system("pause");
    return 0;
}

int ReferenceArrayOfValues (int InputArray[NumberOfLine][NumberOfColumns], int ArrayMinValue, int ArrayMaxValue)
{
	for (int i = 0; i < NumberOfLine; i++)
	{
		for (int j = 0; j < NumberOfColumns; j++)
		{
			InputArray[i][j] = ArrayMinValue + rand() % (ArrayMaxValue + abs(ArrayMinValue));
		}
	}
	return 0;
}

int WriteArray(int InputArray[][NumberOfColumns])
{
	for (int i = 0; i < NumberOfLine; i++)
	{
		for (int j = 0; j < NumberOfColumns; j++)
		{
			cout.width(4);
			cout << InputArray[i][j];
		}
		cout << endl;
		cout << endl;
	}
	return 0;
}

elementProperties FindPropertiesOfMinElementOnSecDiagonal (int InputArray[NumberOfLine][NumberOfColumns])
{
	elementProperties minimalElement;
	minimalElement.value = InputArray[0][NumberOfColumns-1];
	for (int i = 0; i < NumberOfLine; i++)
	{
		for (int j = 0; j < NumberOfColumns; j++)
		{
			if (i + j + 2 == NumberOfLine + 1)
			{
				if (InputArray[i][j] < minimalElement.value)
				{
					minimalElement.lineCoordinate = i + 1;
					minimalElement.columnCoordinate = j + 1;
					minimalElement.value = InputArray[i][j];
				}
			}
		}
	}
	return minimalElement;
}

elementProperties FindPropertiesOfMinElementUnderSecDiagonal(int InputArray[NumberOfLine][NumberOfColumns])
{
	elementProperties minimalElement;
	minimalElement.value = InputArray[0][NumberOfColumns - 1];
	for (int i = 0; i < NumberOfLine; i++)
	{
		for (int j = NumberOfColumns - 1; j > (NumberOfColumns - i - 1); j--)
		{
			if (InputArray[i][j] < minimalElement.value)
			{
				minimalElement.lineCoordinate = i + 1;
				minimalElement.columnCoordinate = j + 1;
				minimalElement.value = InputArray[i][j];
			}
		}
	}
	return minimalElement;
}

void ReplaceElements(int InputArray[NumberOfLine][NumberOfColumns], elementProperties replaceableElement, elementProperties replacingElement)
{
	InputArray[replaceableElement.lineCoordinate - 1][replaceableElement.columnCoordinate - 1] = replacingElement.value;
}

void WritePropertiesOfElement (elementProperties Element, char specialProperty[])
{
	cout<<"Coordinates of the "<<specialProperty<<" element: ("<<Element.lineCoordinate<<", "<<Element.columnCoordinate<<")."<<endl;
	cout<<"Value of the "<<specialProperty<<" element: "<<Element.value<<endl;
	cout<<endl;
}
